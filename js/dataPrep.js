jsn = {
    "rows":[
       [
          {
             "value":"instagram",
             "formattedValue":"instagram"
          },
          {
             "value":"WWDITS Interest",
             "formattedValue":"WWDITS Interest"
          },
          {
             "value":0.07298440065681445,
             "formattedValue":"$0.07"
          },
          {
             "value":0.02,
             "formattedValue":"$0.020"
          }
       ],
       [
          {
             "value":"instagram",
             "formattedValue":"instagram"
          },
          {
             "value":"Comedians and Stand-up",
             "formattedValue":"Comedians and Stand-up"
          },
          {
             "value":0.04990626732035593,
             "formattedValue":"$0.05"
          },
          {
             "value":0.02,
             "formattedValue":"$0.020"
          }
       ],
       [
          {
             "value":"instagram",
             "formattedValue":"instagram"
          },
          {
             "value":"Comp Shows / Interests & Mockumentaries",
             "formattedValue":"Comp Shows / Interests & Mockumentaries"
          },
          {
             "value":0.03319994223334813,
             "formattedValue":"$0.03"
          },
          {
             "value":0.02,
             "formattedValue":"$0.020"
          }
       ],
       [
          {
             "value":"instagram",
             "formattedValue":"instagram"
          },
          {
             "value":"Clever Comedies",
             "formattedValue":"Clever Comedies"
          },
          {
             "value":0.033187333141279425,
             "formattedValue":"$0.03"
          },
          {
             "value":0.02,
             "formattedValue":"$0.020"
          }
       ],
       [
          {
             "value":"instagram",
             "formattedValue":"instagram"
          },
          {
             "value":"S1 Viewers + Retargeting",
             "formattedValue":"S1 Viewers + Retargeting"
          },
          {
             "value":0.026783110500494513,
             "formattedValue":"$0.03"
          },
          {
             "value":0.02,
             "formattedValue":"$0.020"
          }
       ],
       [
          {
             "value":"instagram",
             "formattedValue":"instagram"
          },
          {
             "value":"Broad 18-49",
             "formattedValue":"Broad 18-49"
          },
          {
             "value":0.02311144410642335,
             "formattedValue":"$0.02"
          },
          {
             "value":0.02,
             "formattedValue":"$0.020"
          }
       ],
       [
          {
             "value":"facebook",
             "formattedValue":"facebook"
          },
          {
             "value":"WWDITS Interest",
             "formattedValue":"WWDITS Interest"
          },
          {
             "value":0.019514440245388738,
             "formattedValue":"$0.02"
          },
          {
             "value":0.05,
             "formattedValue":"$0.050"
          }
       ],
       [
          {
             "value":"facebook",
             "formattedValue":"facebook"
          },
          {
             "value":"Comedians and Stand-up",
             "formattedValue":"Comedians and Stand-up"
          },
          {
             "value":0.016852570967206288,
             "formattedValue":"$0.02"
          },
          {
             "value":0.05,
             "formattedValue":"$0.050"
          }
       ],
       [
          {
             "value":"facebook",
             "formattedValue":"facebook"
          },
          {
             "value":"Comp Shows / Interests & Mockumentaries",
             "formattedValue":"Comp Shows / Interests & Mockumentaries"
          },
          {
             "value":0.015183095386402005,
             "formattedValue":"$0.02"
          },
          {
             "value":0.05,
             "formattedValue":"$0.050"
          }
       ],
       [
          {
             "value":"facebook",
             "formattedValue":"facebook"
          },
          {
             "value":"Clever Comedies",
             "formattedValue":"Clever Comedies"
          },
          {
             "value":0.013871060333829907,
             "formattedValue":"$0.01"
          },
          {
             "value":0.05,
             "formattedValue":"$0.050"
          }
       ],
       [
          {
             "value":"facebook",
             "formattedValue":"facebook"
          },
          {
             "value":"S1 Viewers + Retargeting",
             "formattedValue":"S1 Viewers + Retargeting"
          },
          {
             "value":0.012331423105078572,
             "formattedValue":"$0.01"
          },
          {
             "value":0.05,
             "formattedValue":"$0.050"
          }
       ],
       [
          {
             "value":"facebook",
             "formattedValue":"facebook"
          },
          {
             "value":"Broad 18-49",
             "formattedValue":"Broad 18-49"
          },
          {
             "value":0.010650090503293748,
             "formattedValue":"$0.01"
          },
          {
             "value":0.05,
             "formattedValue":"$0.050"
          }
       ]
    ],
    "fields":[
       {
          "type":"dimension",
          "systemName":"MEDIA_BUY_CUSTOM_ATTRIBUTE5",
          "name":"Media Buy Custom Attribute 05",
          "defaultName":"Media Buy Custom Attribute 05"
       },
       {
          "type":"dimension",
          "systemName":"CALC_DIM_877543",
          "name":"Combined Audience",
          "defaultName":"Combined Audience - Calculated"
       },
       {
          "type":"metric",
          "systemName":"DELIVERY_CPV",
          "name":"CPV",
          "defaultName":"CPV"
       },
       {
          "type":"metric",
          "systemName":"CALC_1941408",
          "name":"Benchmark_Test_Lalit",
          "defaultName":"Benchmark_Test_Lalit - Calculated"
       }
    ],
    "totals":[
       {
          "fields":[
             {
                "field":"DELIVERY_CPV",
                "type":"MEASUREMENT"
             },
             {
                "field":"CALC_1941408",
                "type":"MEASUREMENT"
             }
          ],
          "data":[
             [
                "$0.02",
                "$0.020"
             ]
          ]
       }
    ]
 };

let rows = jsn.rows;

let labels = [];
let audience = [];
let benchMark = [];


function reshapeJSON(rows, callback){
   var tmpAudience = [];

   rows.forEach(function(d){ labels.push(d[0].value);});
   labels = labels.filter(function (x, i, a) { return a.indexOf(x) === i; });
   rows.forEach(function(d){ tmpAudience.push(d[1].value); });
   tmpAudience = tmpAudience.filter(function (x, i, a) { return a.indexOf(x) === i; });
   tmpAudience.forEach(function(d) { audience.push({ 'label': d }); })

   callback(rows, labels, audience, cleanData);

    return {
        'labels': labels,
        'series': audience,
        'benchMark': benchMark
    };
};


function getValues(rows, labels, audience, callback) {

   for (let k = 0, klen = labels.length; k < klen; k++) {    
      audience.forEach(function(d, j) {
         for (let i = 0, len = rows.length; i < len; i++) {   
               if ((rows[i][1].value === d.label) && (rows[i][0].value === labels[k])) {
                  benchMark.push(rows[i][3].value.toFixed(2))
                  if (k === 0) {
                     audience[j]['values'] = [rows[i][2].value.toFixed(2)]
                  } else {
                     audience[j]['values'].push(rows[i][2].value.toFixed(2))
                  }
               }
         }         
      });
   };

   callback(audience, labels);
};


function cleanData(data, labels) {
   for(var i = 0; i < data.length; i++) {
      if(data[i].values.length != labels.length) { data.splice(i, 1); }
  }
};

let data = reshapeJSON(rows, getValues);
