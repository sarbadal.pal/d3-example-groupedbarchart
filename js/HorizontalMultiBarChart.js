// var data = {
//     labels: ['Snapchart', 'Twitter', 'Instagram', 'Facebook'],
//     series: [
//         {
//             label: '2013',
//             values: [5, 3, 4, 7]
//         },
//         {
//             label: '2014',
//             values: [2, 5, 6, 2]
//         },
//         {
//             label: '2015',
//             values: [5, 4, 8, 9]
//         },
//     ],
//     benchMark: [6, 3, 5, 4, 5, 3, 5, 7, 5, 4, 5, 3]
// };

var colors = [
    '#000000', 
    '#00008B', 
    '#0000EE', 
    '#003EFF', 
    '#00611C', 
    '#00688B', 
    '#008000', 
    '#008B00', 
    '#009900', 
    '#00AF33', 
    '#00C5CD', 
    '#00CD00'
];



var chartWidth = 500,
    barHeight  = 15,
    groupHeight = barHeight * data.series.length,
    gapBetweenGroups = 10,
    spaceForLabels = 200,
    spaceForLegend = 200;

// Zip the series data together (first values, second values, etc.)
var zippedData = [];
for (var i=0; i<data.labels.length; i++) {
  for (var j=0; j<data.series.length; j++) {
    zippedData.push(data.series[j].values[i]);
  }
}

// Color scale
var barsInGroup = data.series.length;
var chartHeight = barHeight * zippedData.length + gapBetweenGroups * data.labels.length + 20;
var chartHeightMinus = barHeight * zippedData.length + gapBetweenGroups * data.labels.length ;

var x = d3.scale.linear()
    .domain([0, d3.max(zippedData)])
    .range([0, chartWidth]);

var bm = d3.scale.linear()
    .domain([0, d3.max(zippedData)])
    .range([0, chartWidth]);

var y = d3.scale.linear()
    .range([chartHeight - 20 + gapBetweenGroups, 0]);

var yAxis = d3.svg.axis()
    .scale(y)
    .tickFormat('')
    .tickSize(0)
    .orient("left");

var xAxis = d3.svg.axis()
    .scale(x)
    .ticks(10)
    // .attr("fill", function(d,i) { return '#1C39BB'; })
    // .tickFormat(d3.format(",.0f"))
    .orient("bottom");

// Specify the chart area and dimensions
var chart = d3.select(".chart")
    .attr("width", spaceForLabels + chartWidth + spaceForLegend)
    .attr("height", chartHeight);

// Create bars
var bar = chart.selectAll("g")
    .data(zippedData)
    .enter().append("g")
    .attr("transform", function(d, i) {
      return "translate(" + spaceForLabels + "," + (i * barHeight + gapBetweenGroups * (0.5 + Math.floor(i/data.series.length))) + ")";
    });

// Create rectangles of the correct width
bar.append("rect")
    .attr("fill", function(d,i) { 
        if ( i < barsInGroup) {
            return colors[i];
        } else {
            let dx = (i % barsInGroup);
            return colors[dx];
        }
        
    })
    .attr("class", "bar")
    .attr("width", x)
    .attr("height", barHeight - 1);

// Create rectangles of Benchmark
// bar.append("rect")
//     .attr("fill", function(d,i) { return '#691F01'; })
//     .attr("x", function(d,i) { return x(data.benchMark[i]) - 10; })
//     .attr("y", 0 )
//     .attr("width", 3)
//     .attr("height", barHeight - 1);

bar.append("circle")
    .attr("cx", function(d,i) { return x(data.benchMark[i]); })
    .attr("cy", barHeight / 2)
    .attr("r", barHeight / 5)
    .style("fill", function(d,i) { return 'red'; });

// Add text label in bar
bar.append("text")
    .attr("x", function(d) { return x(d) - 3; })
    .attr("y", barHeight / 2)
    // .style("fill", "red")
    .attr("dy", ".35em")
    .text(function(d) { return d; });


bar.append("text")
    .attr("x", function(d) { return x(0) - 5; })
    .attr("y", barHeight / 2)
    .attr("dy", ".35em")
    .style('fill', '#003153')
    .text(function(d,i) { 
        // return data.series[i].label; 
        if ( i < barsInGroup) {
            return data.series[i].label; 
        } else {
            let dx = (i % barsInGroup);
            return data.series[dx].label; 
        }
    });


// Draw labels
bar.append("text")
    .attr("class", "label")
    .attr("x", function(d) { return - 100; })
    .attr("y", groupHeight / 2)
    .style("fill", "#734A12")
    .attr("stroke", function(d,i) { return '#734A12'; })
    .attr("stroke-width", function(d){ return 1; })
    .attr("dy", ".35em")
    .text(function(d,i) {
      if (i % data.series.length === 0) {
        return data.labels[Math.floor(i/data.series.length)];
      } else { return ""; }
    });

chart.append("g")
    .attr("class", "y axis")
    .attr("transform", "translate(" + spaceForLabels + ", " + -gapBetweenGroups/2 + ")")
    .call(yAxis);

chart.append("g")
    .attr("class", "x axis")
    .attr("stroke", function(d,i) { return '#000'; })
    .attr("stroke-width", function(d){ return 0.7; })
    .attr("opacity", 1)
    .attr("transform", "translate(" + spaceForLabels + ", " + chartHeightMinus + ")")
    .call(xAxis);

// Draw legend
var legendRectSize = 15,
    legendSpacing  = 4;

// var legend = chart.selectAll('.legend')
//     .data(data.series)
//     .enter()
//     .append('g')
//     .attr('transform', function (d, i) {
//         var height = legendRectSize + legendSpacing;
//         var offset = -gapBetweenGroups/2;
//         var horz = spaceForLabels + chartWidth + 40 - legendRectSize;
//         var vert = i * height - offset;
//         return 'translate(' + horz + ',' + vert + ')';
//     });

// legend.append('rect')
//     .attr('width', legendRectSize)
//     .attr('height', legendRectSize)
//     .attr("fill", function(d,i) { 
//         if ( i < barsInGroup) {
//             return colors[i];
//         } else {
//             let dx = (i % barsInGroup);
//             return colors[dx];
//         }
        
//     })

// legend.append('text')
//     .attr('class', 'legend')
//     .attr('x', legendRectSize + legendSpacing)
//     .attr('y', legendRectSize - legendSpacing)
//     .text(function (d) { return d.label; });